#!/usr/bin/env python3

# author: Stefan Kurtz

import sys, argparse
import numpy as np
from ete3 import Tree
from dist_matrix import DistanceMatrix

class NJnode:
  def __init__(self):
    self._leftchild = None
    self._rightchild = None
    self._leftdist = None
    self._rightdist = None
  def leftchild_set(self,leftchild):
    self._leftchild = leftchild
  def leftchild(self):
    return self._leftchild
  def rightchild_set(self,rightchild):
    self._rightchild = rightchild
  def rightchild(self):
    return self._rightchild
  def leftdist_set(self,leftdist):
    self._leftdist = leftdist
  def leftdist(self):
    return self._leftdist
  def rightdist_set(self,rightdist):
    self._rightdist = rightdist
  def rightdist(self):
    return self._rightdist

class NeighborJoining:
  def __init__(self,dm):
    self._dm = dm
    self._num_of_taxa = dm.num_of_taxa()
    self._numofnodes = 2 * self._num_of_taxa - 2
    self._bigmatrix = np.zeros((self._numofnodes,self._numofnodes))
    self._nodes = list()
    self._finalnodeA = None
    self._finalnodeB = None
    self._finaldist = None
    for i in range(0,self._numofnodes):
      self._nodes.append(NJnode())
      for j in range(0,i):
        if i < self._num_of_taxa:
          dist = dm.distance(i,j)
          #for andi:
          if str(dist) in ["nan"]:
            dist = 1.0
          assert dist >= 0
        else:
          dist = None
        self._bigmatrix[j,i] = dist
        self._bigmatrix[i,j] = dist
  def taxon_name(self,idx):
    return self._dm.taxon_name(idx)[:10]
  def numofnodes(self):
    return self._numofnodes
  def num_of_taxa(self):
    return self._num_of_taxa
  def __getitem__(self,idx):
    assert idx < len(self._nodes)
    return self._nodes[idx]
  def distance(self,i,j):
    assert i != j
    dist = self._bigmatrix[i,j]
    assert not (dist is None), 'for {} {}'.format(i,j)
    return dist
  def distance_set(self,i,j,d):
    assert i != j
    self._bigmatrix[j,i] = d
    self._bigmatrix[i,j] = d

def update_Rtab(nejo,rtab,is_active,active_nodes):
  assert active_nodes >= 3
  for i in range(nejo.numofnodes()):
    if is_active[i]:
      rvalue = 0.0
      for j in range(nejo.numofnodes()):
        if j != i and is_active[j]:
          rvalue += nejo.distance(i,j)
      rtab[i] = rvalue/(active_nodes - 2)

def gt_neighborjoining_compute(nejo):
  newnodenum = nejo.num_of_taxa()
  assert nejo.num_of_taxa() <= nejo.numofnodes()
  is_active = ([True] * nejo.num_of_taxa()) + \
              ([False] * (nejo.numofnodes() - nejo.num_of_taxa()))
  activenodes = nejo.num_of_taxa()

  # the neighbor joining takes num_of_taxa - 2 steps
  rtab = [None] * nejo.numofnodes()
  for step in range(nejo.num_of_taxa() - 2):
    mindist = None
    min_i = None
    min_j = None
    update_Rtab(nejo, rtab, is_active, activenodes)
    # determine two nodes for which the distance is minimal
    for i in range(1,nejo.numofnodes()):
      if is_active[i]:
        # this node exists, check the distances
        for j in range(i):
          if is_active[j]:
            this_dist = nejo.distance(i,j) - (rtab[i] + rtab[j])
            if mindist is None or this_dist < mindist:
              mindist = this_dist
              min_i = i
              min_j = j
    # add new node to L and remove the daughters
    assert not min_i is None
    assert not min_j is None
    assert min_j < min_i
    is_active[newnodenum] = True
    is_active[min_i] = is_active[min_j] = False
    assert activenodes > 0
    activenodes -= 1

    # save the new node
    min_i_min_j_dist = nejo.distance(min_i,min_j)
    nejo[newnodenum].leftchild_set(min_i)
    nejo[newnodenum].rightchild_set(min_j)
    nejo[newnodenum].leftdist_set((min_i_min_j_dist + rtab[min_i] 
                                                    - rtab[min_j])/2)
    nejo[newnodenum].rightdist_set(min_i_min_j_dist - 
                                   nejo[newnodenum].leftdist())

    # update the distances
    for m in range(newnodenum):
      if is_active[m]:
        nejo.distance_set(newnodenum,m,(nejo.distance(m,min_i) + \
                                        nejo.distance(m,min_j) - \
                                        min_i_min_j_dist)/2)
    newnodenum += 1

  # now only two nodes are active, save them and the distance between them
  for i in range(nejo.numofnodes()):
    if is_active[i]:
      nejo._finalnodeA = i
      break
  nejo._finalnodeB = nejo.numofnodes() - 1
  nejo._finaldist = nejo.distance(nejo._finalnodeB,nejo._finalnodeA)

def prec_str(precision):
  return '{:.' + str(precision) + 'f}'

def to_newick_node(output,nejo,nodenum,precision):
  if nejo[nodenum].leftchild() is None:
    assert nejo[nodenum].rightchild() is None
    output.append('({}'.format(nejo.taxon_name(nodenum)))
    return
  assert not nejo[nodenum].rightchild() is None
  assert nodenum >= nejo.num_of_taxa()
  children = [nejo[nodenum].leftchild(),nejo[nodenum].rightchild()]
  for idx in range(2):
    if idx == 0:
      output.append('(')
    if idx == 0:
      dist = nejo[nodenum].leftdist() 
    else:
      dist = nejo[nodenum].rightdist()
    prc = prec_str(precision)
    if children[idx] < nejo.num_of_taxa():
      output.append(('{}:' + prc).format(nejo.taxon_name(children[idx]),dist))
    else:
      to_newick_node(output,nejo,children[idx],precision)
      output.append(('):' + prc).format(dist))
    if idx == 0:
      output.append(',')

def tree2newick(output,nejo,precision):
  to_newick_node(output,nejo, nejo._finalnodeA,precision)
  output.append(',')
  to_newick_node(output,nejo, nejo._finalnodeB,precision)
  prc = prec_str(precision)
  output.append(('):' + prc + ');').format(nejo._finaldist))


def parse_arguments():
  p = argparse.ArgumentParser(description=('determine phylogenetic tree using '
                                           'the neighbor joining algorithm'))
  p.add_argument('-p','--precision',type=int,default=5,
                  help='specify precision of floats output (default: 5)')
  p.add_argument('--pdf',action='store_true',default=False,
                 help='show tree as PDF file')
  p.add_argument('inputfile',type=str,
                  help='file with distance matrix')
  return p.parse_args()

if __name__ == '__main__':
  args = parse_arguments()
  dm = DistanceMatrix(args.inputfile)
  nejo = NeighborJoining(dm)
  gt_neighborjoining_compute(nejo)
  output = list()
  tree2newick(output,nejo,args.precision)
  newick_string = ''.join(output)
  if args.pdf:
    outfilename = '{}.pdf'.format(args.inputfile)
    this_tree = Tree(newick_string)
    this_tree.render(outfilename)
  else:
    print(newick_string)
