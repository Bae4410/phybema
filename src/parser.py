#!/usr/bin/env python3

'''
Script to parse the phybema options and
to create a datatuplelist with the datasets and reftrees.

author: Birgitta Päuker
'''

import argparse, os, sys, re, subprocess
from argparse import RawTextHelpFormatter
from pathlib import Path
from estimators import DistanceEstimator, estimator_choices
from root_dir import phybema_root_dir

FASTA_SUFFIXES = [".fasta",".faa",".fna",".fasta.gz"]
REFTREE_SUFFIXES = [".nh",".tre"]

# Function to parse the phybema options
def parse_command_line():
  p = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
  root_dir = phybema_root_dir()
  possible_tools = estimator_choices(root_dir,None,None,True)
  p.add_argument("datasetpaths", nargs = '+',
                 help=("Specify a list of paths to directories which contain\n"
                       "a datafile and optionally a reference tree.\n"
                       "A datafile must be in FASTA format ({}),\n"
                       "a reference tree must be in newick format ({}).\n"
                       "All genomes must be contained in one FASTA file.\n"
                       "For datafiles with a reference tree the resulting\n"
                       "NJ-tree (derived from the distances delivered by\n"
                       "the corresponding tool) will be compared to the\n"
                       "reference tree.\n"
                       "The genome names will be cut to 10 characters. \n"
                       "The reference tree must contain the \n"
                       "exact genome names resulting\n"
                       "For datafiles without reference and if at least\n"
                       "two tools have been specified, the resulting\n"
                       "NJ-trees are compared against each other."
                          .format(','.join(FASTA_SUFFIXES),
                                  ','.join(REFTREE_SUFFIXES))))

  p.add_argument("--tools",nargs='+',choices=possible_tools,
                 help=("Specify the tools to evaluate.\n"
                       "If this is the last option, then use -- before\n"
                       "specifying any data set.\n"
                       "default: use all tools."))
  p.add_argument("-o", "--out_treefile_suffix", type=str,default="pdf",
                 choices=["pdf","png","svg","none"],
                 help=("Specify suffix of output files\n"
                       "with the NJ-trees computed from the distance\n"
                       "matrices delivered by each tool. The argument \"none\"\n"
                       "means that no graphic-file is generated. This is\n"
                       "useful for remote sessions, where this does not work\n"
                       "anyway, as the ete toolkit, which generates the\n"
                       "graphics output requires Qt and an X-server\n"
                       "default: pdf"))
  p.add_argument("-d", "--dist", nargs = '*',metavar="metric",
                 help=("Specify the distance metrics to use for comparing\n"
                       "the resulting NJ-trees with the reference tree.\n"
                       "More than one metric is possible.\n"
                       "Possible metrics are:\n"
		       "ms\tMatching Split metric\n"
                       "pd\tPath Difference metric\n"
                       "qt\tQuartet metric\n"
                       "nrf\tnormalised Robinson-Foulds distance\n"
                       "rf\tRobinson-Foulds distance\n"
                       "ms, pd, qt and rf are computed by TreeCmp,\n"
                       "nrf is computed by using the ETE3 toolkit\n"
                       "default: use nrf only\n"))
  p.add_argument("-m","--mat", action='store_true',
                 help=("Write the distance matrices originally computed by\n"
                       "the different programs into out."))
  if len(sys.argv)==1:
    p.print_help(sys.stderr)
    exit(1)

  args = p.parse_args()
  return args

'''
Function to create a datatuplelist with the datafiles
and their reftree if given
'''
def create_datatuplelist(datasetpaths):
  datatuplelist = []

  # Read in the datafile and reftree file from given datadir
  for datasetpath in datasetpaths:

    # Check if there is a directory at datasetpath
    if not os.path.isdir(datasetpath):
      sys.stderr.write("{}: {} is not a directory.\n"
                       .format(sys.argv[0], datasetpath))
      exit(1)

    # Create list of files with FASTA_SUFFIXES suffix
    datafilepaths = list()
    for f in os.scandir(datasetpath):
      if Path(f).is_file:
        for suffix in FASTA_SUFFIXES:
          if re.search(r'{}$'.format(suffix),f.path):
            datafilepaths.append(f.path) 

    # Check if a datafile was found
    if not datafilepaths:
      sys.stderr.write("{}: No file with suffix {}\nin {} found\n"
                       .format(sys.argv[0], FASTA_SUFFIXES, datasetpath))
      exit(1)
    # Check if more than one datafile was found
    elif len(datafilepaths) > 1:
      sys.stderr.write("{}:There is more than one file with a {} suffix "
                       "in directory {}\n"
                       .format(sys.argv[0], FASTA_SUFFIXES, datasetpath))
      exit(1)
    else:
      datafile = datafilepaths[0]
      # Check if found file is empty
      if os.path.getsize(datafile) == 0:
        sys.stderr.write("{}: The found datafile {} is empty.\n"
                         .format(sys.argv[0], datafile))
        exit(1)
      mo = re.search(r'(.*)\.gz$',datafile)
      if mo:
        uncompressed_file = "{}".format(mo.group(1))
        if not os.path.isfile(uncompressed_file):
          call_list = ['gzip','-k','-d',datafile]
          try:
            output = subprocess.run(call_list,stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE, 
                                    universal_newlines=True)
          except: 
            sys.stderr.write('{}: executing {} failed\n'
                              .format(sys.argv[0],' '.join(call_list)))
            exit(1)
        datafile = uncompressed_file

    # Create list of files with REFTREE_SUFFIXES suffix
    reftreefilepaths = [f.path for f in os.scandir(datasetpath) if
                        Path(f).is_file and os.path.splitext(f)[1]
                        in REFTREE_SUFFIXES]

    # Check if a reftree was found
    if not reftreefilepaths:
      reftree = None
    # Check if more than one reftree was found
    elif len(reftreefilepaths) > 1:
      sys.stderr.write("{}: There is more than one file with a {} suffix "
                       "in directory {}\n"
                       .format(sys.argv[0], REFTREE_SUFFIXES, datasetpath))
      exit(1)
    else:
      reftree = reftreefilepaths[0]
      # Check if found file is empty
      if os.path.getsize(reftree) == 0:
        sys.stderr.write("{}: The found reftreefile {} is empty.\n"
                         .format(sys.argv[0], reftree))
        exit(1)

      sys.stdout.write("# reftree {} found.\n# It will be used for comparison."
                       "\n\n".format(reftree))

    # Insert datatuple into datatuplelist
    datatuple = datafile,reftree
    datatuplelist.append(datatuple)

  return datatuplelist
