#!/usr/bin/env python3

'''
helper script for getting the root directory of phybema

'''

import os.path

def phybema_root_dir():
  # Path to this dir
  this_dir = os.path.dirname(os.path.abspath(__file__))
  # Path to the root dir
  return os.path.split(this_dir)[0]
