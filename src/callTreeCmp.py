#!/usr/bin/env python3

'''
Skript for calling TreeCmp (https://eti.pg.edu.pl/treecmp/)
Descripted in the paper "TreeCmp: Comparison of Trees in Polynomial Time"
(doi: 10.4137/EBO.S9657)

To call the programs run from the subprocess module is used.
Run requires python version >= 3.5

author: Birgitta Päuker
'''

import subprocess, sys, os
import openFile

'''
Function for calling TreeCmp with the generated concatenated newickfile.
The Program will be called with the -r option if reftree is not None and
with option -m otherwise.
The program is called with the -N option to additionaly get the distances
scaled by the average distance to random trees with the same number of leaves
'''
def run_TreeCmp(tcdistmetriclist,reftree, njconcatfilepath,
                tcoutfilepath, root_dir):
  # Create string with the given distance metrics
  diststr = ""
  for dist in tcdistmetriclist:
    diststr = diststr + dist
    if not dist == tcdistmetriclist[-1]:
      diststr = diststr + " "
      
  # Check if njconcatfilepath is empty
  if os.path.getsize(njconcatfilepath) == 0:
    sys.stderr.write("{}: The newickfile {} is empty.\n"
                         .format(sys.argv[0], njconcatfilepath))
    exit(1)
        
  # Programm parameters
  tcmp_path = root_dir + \
              "/src_external/TreeCmp_v1.1-b308/TreeCmp/bin/TreeCmp.jar"
  if reftree:
    arg = ["java", "-jar", tcmp_path,"-N","-r", 
           "{}".format(reftree), 
           "-d",
           "{}".format(diststr), 
           "-i", "{}".format(njconcatfilepath),"-o", 
           "{}".format(tcoutfilepath)]
  else:
    arg = ["java", "-jar", tcmp_path,"-N","-m", "-d", 
           "{}".format(diststr),
           "-i", 
           "{}".format(njconcatfilepath),
           "-o", 
           "{}".format(tcoutfilepath)]
    
  # Call TreeCmp
  try:
    output = subprocess.run(arg,stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE, universal_newlines=True)
  except:
    sys.stderr.write("Calling TreeCmp failed\n")
    exit(1)
    
  # Check if an Error turn up while running the programm
  if output.stderr:
    print('{}: an Error at TreeCmp appeard:\n{}'
            .format(sys.argv[0],output.stderr))
    exit(1)
  
  if os.path.getsize(tcoutfilepath) == 0:
        sys.stderr.write("{}: an Error at TreeCmp appeard\n"
                         "The Trees might contain different leaf sets.\n"
                         .format(sys.argv[0]))
        exit(1)
