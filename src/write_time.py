#!/usr/bin/env python3

'''
Script to write a file with the computation times of the estimators.

author: Birgitta Päuker
'''

import os
import openFile

# Function to write the computation times into a file
def write_time(timefilepath, timelist, datafilename):
  programs = []
  times = []
  
  timefile = openFile.open_file(timefilepath, "w")
  
  for progname,time in timelist:
    programs.append(progname)
    times.append(time)
  
  # Write estimators
  progstr = "Dataset"
  for prog in programs:
    progstr = progstr + "\t{}".format(prog)
  progstr = progstr + "\n"
  timefile.write(progstr)
  
  # Write times
  timestr = "{}".format(datafilename)
  for time in times:
    timestr = timestr + "\t{}".format(time)
  timefile.write('{}\n'.format(timestr))
  
  # Close file
  timefile.close()
