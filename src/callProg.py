#!/usr/bin/env python3

'''
Script for calling the, in estimator.py specified estimators
using the given path.

To call the programs run from the subprocess module is used.
Run requires python version >= 3.5

author: Birgitta Päuker
'''

import subprocess, sys, os, shutil
from datetime import datetime
import openFile

'''
Function to call the given programs with their given path. 
The output is writen into a file at the progoutfilepath. 
If the program writes its output into a file 
the content will be copied into the progoutfilepath
'''
def run_prog(progname,call_list,fixed_out_file_name,progoutfilepath):
  
  sys.stderr.write("# starting program {}\n".format(progname))
  start_time = datetime.now()
  
  sys.stderr.write('# {}\n'.format(' '.join(call_list)))
  try:
    output = subprocess.run(call_list,stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE, universal_newlines=True)
  except: 
    sys.stderr.write("calling program {} failed\n".format(progname))
    exit(1)
    
  end_time = datetime.now()

  '''
  Check if an Error turn up while running the program
  andi writes to stderr even if there is an output, 
  therefore test if stdout is empty
  '''
  if output.returncode != 0 and not output.stdout:
    sys.stderr.write("{}: error when executing {}:\n{}\n"
                     .format(sys.argv[0], progname, output.stderr))
    exit(1)

  sys.stderr.write("# program {} complete\n".format(progname))
  time_taken = end_time - start_time
  sys.stderr.write("# runtime: {}\n".format(time_taken))
  
  '''
  if the program writes its output into a file with fixed name
  copy content and remove the original, otherwise create new file
  and redirect stdout to this file.
  '''
  if fixed_out_file_name:
    # Check for the outfile
    if not os.path.isfile(fixed_out_file_name):
      sys.stderr.write("There is no output file {} for program {}\n"
                       .format(fixed_out_file_name,progname))
      exit(1)
    # Copy the outfile with preserved metadata into progoutfilepath
    shutil.copy2(fixed_out_file_name, progoutfilepath)

    # Remove the outfile of the program to prevent further complications
    # with other programs
    os.remove(fixed_out_file_name)
  else:
    # Create an outfile:
    progoutfile = openFile.open_file(progoutfilepath, "w")
    progoutfile.write(output.stdout)
    progoutfile.close()
  return time_taken
