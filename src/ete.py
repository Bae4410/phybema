#!/usr/bin/env python3

'''
Script to call functions from the ete toolkit (http://etetoolkit.org/)

author: Birgitta Päuker
'''

from ete3 import Tree, TreeStyle
import sys
import argparse

# Function to load a tree structure from a newick file
def load_tree(treefile):
  try:
    t = Tree(treefile)
  except IOError as err:
    sys.stderr.write("Can't open file {}: {}\n".format(treefile,err))
    exit(1)
  return t

'''
Function to generate a pdf, svg or png of a tree 
created from a given newick file
'''
def show_tree(newickfilepath, outtreefilepath):
  t = load_tree(newickfilepath)
  ts = TreeStyle()
  ts.scale = 100
  ts.show_branch_length = True
  t.render(outtreefilepath, tree_style = ts)

# Function to determine the normalised robinson foulds distance 
def robinson_foulds_ete(t,t2):
  # returns list with rf, max_rf, common_leaves, parts_t, parts_t2
  rf = t.robinson_foulds(t2, unrooted_trees = True)
  if(rf[0] == 0):
    nrf = 0.0
  else:
    nrf = rf[0]/rf[1]
  return rf[0], rf[1], nrf

'''
Function to compare a tree given from a newick file with a reference tree
The used distance is the normalised robinson foulds distance
'''
def compare_tree(newickfile, reftree):
  ref = load_tree(reftree)
  tree = load_tree(newickfile)
  rf = robinson_foulds_ete(tree,ref)
  return rf[2]

'''
Function to create a matrix filled with the normalised robinson foulds 
distances of comparing each newick tree with the others
'''
def compute_nrf_matrix(newickfilelist):
  nrf_matrix = dict()
  for i in range(0, len(newickfilelist)-1):
    linedict = dict()
    for j in range(i+1, len(newickfilelist)):
      nrf = compare_tree(newickfilelist[i], newickfilelist[j])
      linedict[j] = nrf
    nrf_matrix[i] = linedict
  return nrf_matrix
