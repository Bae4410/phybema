#!/usr/bin/env python3

'''
Script to test if needed programs can be found in the path.

author: Birgitta Päuker
'''

import shutil, sys, subprocess, re

# Function to test if needed programs can be found in the path.
def check_req():
  
  # Test if mash (version >=2.1) is in the path
  if not shutil.which('mash'):
    sys.stderr.write("{}: mash is not in your path.\n".format(sys.argv[0]))
    exit(1)
  else:
    try:
      output = subprocess.run(["mash", "--version"],stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE, universal_newlines=True)
    except:
      sys.stderr.write("{}:Checking mash version failed\n".format(sys.argv[0]))
      exit(1)
    if float(output.stdout) < 2.1:
      sys.stderr.write("{}: mash version >= 2.1 needed.\n".format(sys.argv[0]))
      exit(1)
  
  # Test if ete3 is in the path
  if not shutil.which('ete3'):
    sys.stderr.write("{}: ete3 is not in your path.\n".format(sys.argv[0]))
    exit(1)
    
  # Test if python (version >=3.5) is in the path
  if not shutil.which('python3'):
    sys.stderr.write("{}: python3 is not in your path.\n".format(sys.argv[0]))
    exit(1)
  else:
    try:
      output = subprocess.run(["python3", "--version"],stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE, universal_newlines=True)
    except:
      sys.stderr.write("{}: Checking python3 version failed\n"
                       .format(sys.argv[0]))
      exit(1)
    version = list(filter(None, (re.split(r'\s',output.stdout))))[-1]
    versionnum = ".".join(version.split(".")[:2])
    if float(versionnum) < 3.5:
      sys.stderr.write("{}: python3 version >= 3.5 needed.\n"
                       .format(sys.argv[0]))
      exit(1)
