#!/usr/bin/env python3

'''
Script to write a file with the distances determined by TreeCmp
ordered by the estimators.
The script needs the format of the TreeCmp outfile of version 1.1.
'''

import openFile

# Function to write the tablehead with the distance metrics
def print_metrics_tablehead(metricnamelist, outfile, tablehead):
  for met in metricnamelist:
    tablehead = tablehead + "\t{}".format(met)
  outfile.write(tablehead + "\n")
  print(tablehead)

# Function to create a list with the names of the metrics
def get_metricnamelist(tcdistmetriclist, tcoutfilepath, nrf_metric):
  
  # variable declaration:
  metricnamelist = []
  
  # Get the distance metric names
  if tcdistmetriclist:
    # open TreeCmp Outfile
    infile = openFile.open_file(tcoutfilepath)
    lines = infile.readlines()
    infile.close()

    firstlineatt = lines[0].rstrip().split("\t")
    metricnamelist = firstlineatt[3:]

  if nrf_metric:
    metricnamelist.append("normalisedR-F")
    
  return metricnamelist
  

# Function to write the distance file for comparing each tree with the others
def print_matrixcomparison(datafile, distoutfilepath, prognamelist, 
                           tcdistmetriclist, tcoutfilepath, nrf_metric, 
                           nrf_matrix):
  # variable declaration:
  metricnamelist = []
  linecount = 1

  # open the outputfile
  outfile = openFile.open_file(distoutfilepath, "w")
  
  # write the datafile
  outfile.write("datafile: {}\n".format(datafile))
  print("datafile: {}".format(datafile))
  
  # Get the distance metric names
  metricnamelist = get_metricnamelist(tcdistmetriclist,tcoutfilepath, 
                                      nrf_metric)

  # Write the tablehead: Tree1  Tree2   distmetric1 ...
  tablehead = "Program1\tProgram2"
  print_metrics_tablehead(metricnamelist, outfile, tablehead)
  
  # Write Tree1 Tree2 and the distances
  for i in range(0, len(prognamelist)-1):
    tree1 = prognamelist[i]
    for j in range(i+1, len(prognamelist)):
      outline = ""
      tree2 = prognamelist[j]
      outline = outline + "{}\t{}".format(tree1, tree2)
      
      # Get a string with the distances
      if tcdistmetriclist:
        # open TreeCmp Outfile
        infile = openFile.open_file(tcoutfilepath)
        lines = infile.readlines()
        infile.close()
        
        lineatt = lines[linecount].rstrip().split("\t")
        values = lineatt[3:]
        for value in values:
          outline = outline + "\t{}".format(value)
          
      if nrf_metric:
        outline = outline + "\t{}".format(nrf_matrix[i][j])
        
      # Write into the outfile
      outfile.write(outline + "\n")
      print(outline)
      
      linecount = linecount + 1
      
  outfile.close()

# Function to write the distance file for comparing with a reference tree
def print_refcomparison(datafile, reftree, distoutfilepath, prognamelist, 
                        tcdistmetriclist, tcoutfilepath, nrf_metric, nrf_list):
  
  # variable declaration:
  metricnamelist = []

  # open the outputfile
  outfile = openFile.open_file(distoutfilepath, "w")
  
  # write the datafile and reftree names
  outfile.write("datafile:{}\nreftree:{}\n".format(datafile,reftree))
  print("datafile:{}".format(datafile))
  print("reftree:{}".format(reftree))

  # Get the distance metric names
  metricnamelist = get_metricnamelist(tcdistmetriclist, tcoutfilepath, 
                                      nrf_metric)

  # Write the tablehead: Programm   distmetric1 ...
  tablehead = "Program"
  print_metrics_tablehead(metricnamelist, outfile, tablehead)
  
  # Write the programs and their distances
  for prognum in range(0,len(prognamelist)):
    outline = "{}".format(prognamelist[prognum])

    if tcdistmetriclist:
      # open TreeCmp Outfile
      infile = openFile.open_file(tcoutfilepath)
      lines = infile.readlines()
      infile.close()
        
      ls = lines[prognum+1].rstrip("\n").split("\t")
      values = ls[3:]
      for value in values:
        outline = outline + "\t{}".format(value)
        
    if nrf_metric:
      outline = outline + "\t{}".format(nrf_list[prognum])
      
    # Write into the outfile
    outfile.write(outline + "\n")
    print(outline)
    
  outfile.close()
