#!/usr/bin/env python3

'''
helper script for opening a file

author: Birgitta Päuker
'''

import sys

# Function to open a given file for writing or reading
def open_file(filename, mode = 'r'):
  try:
    fp = open(filename,mode)
  except IOError as err:
    sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
    exit(1)
  return fp
