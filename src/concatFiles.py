#!/usr/bin/env python3

'''
helper script to concatenate a list of files into one file 
writen at a given outfilepath.

author: Birgitta Päuker
'''

import argparse, sys
import openFile

'''
Function to concatenate a list of given files into a file
writen at the given outfilepath
'''
def concat_files(infiles, outfilepath):
  if len(infiles) > 1:
    concat = '\n'.join([open(f).read() for f in infiles])
    concatfile = openFile.open_file(outfilepath, 'w')
    concatfile.write(concat)
    concatfile.close()
  else:
    sys.stderr.write("{}:A list of inputfiles is needed to concatenate them.\n"
                     .format(sys.argv[0]))
    exit(1)

