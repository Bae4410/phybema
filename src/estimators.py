#!/usr/bin/env python3

'''
ADD YOUR TOOL TO THIS SCRIPT

Definition of the class DistanceEstimator to handle the specified estimators
and their additional informations

To add your tool to phybema, please insert the needed information at the
marked location in this script.

The given version of kmacs (http://kmacs.gobics.de/) and
fswm (http://fswm.gobics.de/) are edited implementations which can be
found in the folder src_external. The edited implementations allow to specify
the name and path of the generated distance files. They were written to allow
calling phybema parallel.

authors: Stefan Kurtz, Birgitta Päuker
'''

import sys, re, shutil

class DistanceEstimator:
  def __init__(self,call,call_options,fixed_out_file_name,optional_end):
    self._call = call
    if not re.search(r'/',call):
      self._name = call
    else:
      self._name = call.split('/')[-1]
    self._name = re.sub(r'\.sh',"",self._name)
    self._call_options = call_options
    self._fixed_out_file_name = fixed_out_file_name
    self._optional_end = optional_end
  def call_list(self,inputfile):
    l = [self._call] + self._call_options + [inputfile]
    if self._optional_end:
      return l + self._optional_end
    return l
  def call(self):
    return self._call
  def name_set(self,name):
    self._name = name
  def name(self):
    return self._name
  def fixed_out_file_name(self):
    return self._fixed_out_file_name
  def __str__(self):
    lines = list()
    lines.append("name={}".format(self.name()))
    if len(self._call_options) > 0:
      lines.append(' '.join(self._call_options))
    if self._fixed_out_file_name:
      lines.append("fixed_out_file_name={}"
                   .format(self._fixed_out_file_name))
    if self._optional_end:
      lines.append("optional_end={}".format(self._optional_end))
    return '\t'.join(lines)

def estimator_choices(root_dir,choices,dataset, only_names=False):
  andi = DistanceEstimator(call="andi",
                           call_options=["--threads=1"],
                           fixed_out_file_name=None,
                           optional_end=None)
  mash = DistanceEstimator(call="mash",
                           call_options=["triangle"],
                           fixed_out_file_name=None,
                           optional_end=None)

  '''
  ADD YOUR TOOL HERE
  specify new tool here as instance of class DistanceEstimator and
  add the instance to the following list
  '''

  all_estimators = [andi,mash]
  all_estimators = [a for a in all_estimators if shutil.which(a.call())]

  estimator_dict = {est.name() : est for est in all_estimators}
  if choices:
    chosen_estimators = list()
    for c in choices:
      if not (c in estimator_dict):
        sys.stderr.write(('{}: estimator {} is not available, '
                          'possible choices: {}\n')
                          .format(sys.argv[0],c,list(estimator_dict.keys())))
        exit(1)
      chosen_estimators.append(estimator_dict[c])
  else:
    chosen_estimators = all_estimators
  if only_names:
    return [est.name() for est in chosen_estimators]
  else:
    return chosen_estimators
