#!/usr/bin/env python3

'''
helper script to create a directory at a given path

author: Birgitta Päuker
'''

import os,sys

# Function to create a directory at the given path
def create_dir(path):
  try:
    os.makedirs(path)
    print("# directory {} created".format(path))
  except FileExistsError:
    sys.stderr.write("# directory {} already exists\n".format(path))
