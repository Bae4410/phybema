#!/bin/sh

if test $# -ne 1
then
  echo "Usage: $0 <subdir>"
  exit 1
fi

subdir=$1
src/phybema.py -o none --tools mash andi -- testdata/${subdir}/
for prog in andi mash
do
  for suffix in mat nh
  do
    diff temp/${subdir}_${prog}.${suffix} testsuite/${subdir}_${prog}.${suffix}
  done
done
