.PHONY:test clean

test:testsuite/test_rep.sh
	@testsuite/test_rep.sh testset1
	@echo "all tests passed"

test_y:
	src/phybema.py -o none --tools mash andi -- testdata/Yersinia/

test_e:
	src/phybema.py -o none --tools mash andi -- testdata/E_coli/

clean:
	@${RM} -r temp out
	@${RM} -f testdata/E_coli/sequence.fasta
	@${RM} -f testdata/Yersinia/sequence.fasta
